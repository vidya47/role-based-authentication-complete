# README #

This repository contains a role-based authentication system which has a front-end and back end to create a "Todo application which is Shared".

Role based authentication system uses Passport, MEAN stack (MongoDB, Express, Angular(here it is Ionic and Node.js).

Users will be able to login and gain access to a shared list of todos. Three different roles are present (Reader, Creator, Editor).

Clone the repo/download the repo and from the local repo folder, run npm install to load all the dependencies.